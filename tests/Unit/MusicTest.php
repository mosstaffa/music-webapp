<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MusicTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function get_the_latest_musics(){

        // when request fro latest musics
        $response = $this->json('get', '/api/musics/latest');

        // then we should return the latest 50 musics as an array
        $response->assertStatus(200)->assertJson([
            'ok'      => true,
            'status'   => 200
        ]);

    }

}

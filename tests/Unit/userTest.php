<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class userTest extends TestCase
{
    use DatabaseMigrations;
    protected function setup(  ): void
    {
        parent::setUp();
    }

    /** @test */
    public function client_app_can_retrieve_token(){
        // given we have these credentials
        $email = "mostafaa_rahmani@outlook.com";
        $password = 'fklaew4pw2320ldasdalklwkeq23131';
        $appkey = 'sjdlslsdspewlskdjsnvk+/ds/+/dsda/=dsadalklxvls/ds=21ldsk';
        // when we sends login requestd
        $response = $this->json('post', '/api/auth/login', [
            'email' => $email,
            'password' => $password,
            'remember_me'   => true,
            'app_key'   => $appkey
        ]);
        dd( $response->json());
        // then we sould get the token
        $response->assertStatus(200);

    }

}

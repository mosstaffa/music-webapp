<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function makeRespond( $ok, int $status, $message = null, $body = null ){
        return [
            'ok' => $ok,
            'status' => $status,
            'message' => $message,
            'body' => $body
        ];
    }
}

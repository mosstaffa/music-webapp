<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    public function songs()
    {
        return $this->belongsToMany('App\Song');
    }
}

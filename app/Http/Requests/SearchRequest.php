<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     * @return array
     */
    public function messages(){
        return [
            'query.required' => 'متن جستجو ارسال نشد. لطفا دوباره تلاش کنید',
            'query.string'  => 'متن جسجو باید شامل حروف باشد',
            'query.min'     => 'متن جسجو حداقل باید سه حرف باشد'
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => 'required|min:3|string'
        ];
    }
}

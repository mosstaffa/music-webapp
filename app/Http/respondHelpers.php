<?php
/**
* helper methods for handling http responses
*/
function respondBadRequest($message){
    return sendErrorResponse($message, 400);
}
function respondUnauthorized($message){
    return sendErrorResponse($message, 401);
}

function respondForbidden($message){
    return sendErrorResponse($message, 403);
}

function respondNotFound($message){
    return sendErrorResponse($message, 404);
}

function respondServerError($message){
    return sendErrorResponse($message, 500);
}


function respondSuccess($message, $body){
    return sendResponse($message, 200, $body);
}

function respondCreated($message){
    return sendResponse($message, 201, $body);
}

function respondNoContent(){
    return sendResponse(204);
}
/**
* @param string $message
* @param  int $status,
* @return json
 */
function sendErrorResponse(string $message, int $status){
    return response()->json([
        'ok' => false,
        'status' => $status,
        'message' => $message
    ], $status);
}
/**
* @param string $message
* @param  int $status,
* @param body
* @return json
 */
function sendResponse(string $message = null , int $status, $body = null){
    return response()->json([
        'ok' => true,
        'status' => $status,
        'message' => $message,
        'body'  => $body
    ], $status);
}

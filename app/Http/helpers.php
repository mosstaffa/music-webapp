<?php
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\DB;
use Spatie\Valuestore\Valuestore;
if( ! function_exists( 'encodeUrl')){
    function encodeUrl($url){
        try{
            $urlParts = explode('/', $url);
            $utf8Part = '';
            $i = 0;
            foreach ( $urlParts as $part ){
                if($i > 2){
                    $utf8Part .= '/' . $part;
                }
                $i++;
            }
            $link = str_replace(
                trim($utf8Part , '/'),
                rawurlencode(trim($utf8Part, '/')),
                $url);
            return $link;
        }catch ( \Error $e ){
            return respondServerError('مشکلی در محاسبات به وجود آمده. لطفا دوباره تلاش کنید.');
        }
    }
}

if( ! function_exists('getSearchUrl')){
    function getSearchUrl($query, $root){
        $query = trim($query); // trim form space
        $parts = explode(' ', trim($query));
        $newQuery = '';
        foreach($parts as $part )
            $newQuery .= rawurlencode($part) . '+';

        $link = $root . '/?s=' . trim($newQuery, '+');
        return $link;
    }
}

if( ! function_exists('getIranMusicDl') ){
    function getIranMusicDl( $loadLink ){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $loadLink);
        $dom = HtmlDomParser::str_get_html($res->getBody());
        // $song['dl120'] = $article->find('div.content div.dlbox a', 1)->href;
        return $dom->find('div.bxRow div.col-md-4 a', 0)->href;
    }
}

if (! function_exists('getNex1Song')) {
    function getNex1Song(string $link) {
        $client = new GuzzleHttp\Client();
        $response = $client->request('GET', encodeUrl($link));
        $dom = HtmlDomParser::str_get_html($response->getBody());
        // retrieving parts
        $article = $dom->find('div[itemtype=https://schema.org/AudioObject]', 0);
        // $title = $article->find("meta[itemprop=name]", 0)->getAttribute('content');
        // $image = $article->find('meta[itemprop=image]', 0)->getAttribute('content');
        // $singer = $article->find('meta[itemprop=author]', 0)->getAttribute('content');
        // $songName = $article->find('meta[itemprop=about]', 0)->getAttribute('content');
        $dl320 = $article->find('div.pscn div.lnkdl', 0)->firstChild()->getAttribute('href');
        $dl120 = $article->find('div.pscn div.lnkdl', 0)->lastChild()->getAttribute('href');
        return [
            // 'singer' => $singer,
            // 'name'  => $songName,
            // 'title' => $title,
            // 'image' => $image,
            'dl320' => $dl320,
            'dl120' => $dl120
        ];
    }
}

if( ! function_exists('updateWeeklySongs') ){
    function updateWeeklySongs(){
        //clearing the old data
        $valuestore = Valuestore::make(storage_path('app/songs.json'));
        $valuestore->forget('weekly');

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://nex1music.ir/');
        $dom = HtmlDomParser::str_get_html($res->getBody());
        $weeklyList = $dom->find('div.pcblk ul#weekly li a');
        $songs = [];
        foreach ($weeklyList as $item ){
            $song = getNex1Song($item->getAttribute('href'));
            array_push($songs, $song);
        }
        $valuestore->put('weekly', $songs);
        return;
    }
}

if( ! function_exists('updateMonthlySongs') ){
    function updateMonthlySongs(){
        //clearing the old data
        $valuestore = Valuestore::make(storage_path('app/songs.json'));
        $valuestore->forget('monthly');

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://nex1music.ir/');
        $dom = HtmlDomParser::str_get_html($res->getBody());
        $monthlyList = $dom->find('div.pcblk ul#monthly li a');
        $songs = [];
        foreach ($monthlyList as $item ){
            $song = getNex1Song($item->getAttribute('href'));
            array_push($songs, $song);
        }
        $valuestore->put('monthly', $songs);
        return;
    }
}

if( ! function_exists('searchSong') ){
    function searchSong($query){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', getSearchUrl($query, 'http://www.nex1music.ir'));
        $dom = HtmlDomParser::str_get_html($res->getBody());
        $songs = [];
        foreach( $dom->find('div.ps') as $item ){
            if( is_object($item->find('img', 0)) && is_object($item->find('h2', 0)) && is_object($item->find('div.psdown a.more', 0))){
                $song['image']  = $item->find('img', 0)->getAttribute('src');
                $song['title'] = str_replace('دانلود', '',$item->find('h2', 0)->plaintext );
                $song['load_link'] = $item->find('div.psdown a.more', 0)->href;
                $song['website'] = 'https://nex1music.ir';
                if( strpos($song['title'], 'ویدیو') ||  strpos($song['title'], 'ویدئو') )
                    continue;
                array_push($songs, $song);
            }else{
                continue;
            }
        }
        return $songs;
    }
}

if (! function_exists('getPopMusicLinks')) {
    function getPopMusicLinks(string $link) {
        $client = new GuzzleHttp\Client();
        $response = $client->request('GET', $link);
        // return $response;
        $dom = HtmlDomParser::str_get_html($response->getBody());
        // retrieving parts
        $article = $dom->find('.download', 0 );
        if($article == null) { 
            $client = new GuzzleHttp\Client();
            $response = $client->request('GET', encodeUrl($link));
            $dom = HtmlDomParser::str_get_html($response->getBody());
            $article = $dom->find('.download', 0 );
        }
        $downloadLinks = $article->find('a');
        // return $downloadLinks;
        $dl320 = null;
        $dl120 = null;
        foreach ($downloadLinks as $linkTag) {
            if(strpos($linkTag->getAttribute('title'), '320')) {
                $dl320 = $linkTag->getAttribute('href');
            }
            if(strpos($linkTag->getAttribute('title'), '128')) {
                $dl120 = $linkTag->getAttribute('href');  
            }
        }
        return [
            'dl320' => $dl320,
            'dl120' => $dl120
        ];
    }
}

if( ! function_exists('searchPopMusic') ){
    function searchPopMusic($query){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', getSearchUrl($query, 'https://popimusic.com'));
        $dom = HtmlDomParser::str_get_html($res->getBody());
        $songs = [];
        foreach( $dom->find('div.post') as $item ){
            if( is_object($item->find('.foot a', 0)) ){
                $song['image']  = $item->find('img', 0)->getAttribute('src');
                $song['title'] = str_replace('دانلود', '', $item->find('.title h2', 0)->plaintext );
                $song['load_link'] = $item->find('.foot a', 0)->getAttribute('href');
                $song['website'] = 'https://popimusic.com';
                // if( strpos($song['title'], 'ویدیو') ||  strpos($song['title'], 'ویدئو') )
                //     continue;
                array_push($songs, $song);
            }else{
                continue;
            }
        }
        return $songs;
    }
}

<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class Song extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'name'      => $this->songName,
        'singer'    => $this->singer,
        'image'     => $this->image,
        'loadLink'  => isset($this->loadLink) ? $this->loadLink : null,
        'dl320'     => $this->dl320 ? $this->dl320 : null,
        'dl120'     => $this->dl120 ? $this->dl120 : null,
      ];
    }
}

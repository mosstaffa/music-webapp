<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Valuestore\Valuestore;
use Carbon\Carbon;
use App\User;


class AuthController extends Controller
{

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }


    /**
    * @param [string] email,
    * @param [string] password
    * @param [string] remember_me
    * @return [string] acccess_token
    */
    public function login(Request $request){
        $this->validate($request, [
            'email' => 'email|required|string',
            'password'  => 'required|string',
            'remember_me'   => 'boolean',
            'app_key'   => 'string|required'
        ]);
        $credentials = request(['email', 'password']);

        if( ! auth()->attempt($credentials) )
            return respondUnauthorized('Authorization failed.');

        $appKeyFile = Valuestore::make(storage_path('app/appkeys.json'));
        if( $appKeyFile->get('appkey') == bcrypt($request->app_key)  )
            return respondUnauthorized('Authorization failed. app_key is not valid');

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if( $request->remember_me )
            $token->expires_at = Carbon::now()->addweeks(1);
        $token->save();
        return respondSuccess('Logged in successfully', [
            'access_token' => $tokenResult->accessToken,
            'token_type'    => 'Bearer',
            'expires_at'    => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);
    }

    /**
     * Logout user (Revoke the token)
     * @return [string] message
     */
     public function Logout(Request $request)
     {
         $request->user()->token()->revoke();
         return respondSuccess('Logged out successfully');
     }
}

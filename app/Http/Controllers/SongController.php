<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\SongCollection;
use App\Http\Controllers\RespondController;
use App\Http\Requests\SearchRequest;
use Spatie\Valuestore\Valuestore;
class SongController extends Controller
{
    public function weeklySongs()
    {
        try {
            $valuestore = Valuestore::make(storage_path('app/songs.json'));
            if($songs = $valuestore->get('weekly', false)){
                return respondSuccess('Weekly songs returned successfully.', $songs);
            }
        } catch (\Exception $e) {
            return respondServerError('Something went wrong when when getting the weekly songs');
        }
        return respondSuccess('10 best songs of the week', new SongCollection($songs));
    }

    public function monthlySongs()
    {
        try {
            $valuestore = Valuestore::make(storage_path('app/songs.json'));
            if($songs = $valuestore->get('monthly', false)){
                return respondSuccess('Monthly songs returned successfully.', $songs);
            }
        } catch (\Exception $e) {
            return respondServerError('Something went wrong when when collecting the monthly songs');
        }
        return respondSuccess('10 best songs of the month', new SongCollection($songs));

    }

    public function search(SearchRequest $request)
    {
        switch ($request->site) {
            case 'popmusic':
                    $songs = searchPopMusic($request->input('query'));
                    return respondSuccess('Search results returned successfully.', $songs);
            default:
                $songs = searchSong($request->input('query'));
                return respondSuccess('Search results returned successfully.', $songs);
        }
    }

    public function getLinks( Request $request){

        switch ($request->site) {
            case 'popmusic':
                $song = getPopMusicLinks($request->load_link);
                return respondSuccess('Song Details Returned Successfully.', $song);
            default:
                $song = getNex1Song($request->load_link);
                return respondSuccess('Song Details Returned Successfully.', $song);
        }
    }
}

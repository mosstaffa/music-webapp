<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class updateMonthlySongs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateMonthlySongs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It updates the monthly songs in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        updateMonthlySongs();
    }
}

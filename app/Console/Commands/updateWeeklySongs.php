<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class updateWeeklySongs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateWeeklySongs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command runs the updateWeeklySongs function to update the weekly songs in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        updateWeeklySongs();
    }
}

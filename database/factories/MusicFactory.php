<?php

use Faker\Generator as Faker;

$factory->define(App\Music::class, function (Faker $faker) {
    return [
        'query' =>  $faker->word,
        'title' =>  $faker->sentence,
        'image' =>  $faker->imageUrl(640, 480),
        'dlink'  =>
            'http://dl.nex1music.ir/1397/11/30/Chaartaar%20-%20Aavaazeh%20Khaan.mp3?time=1551282613&filename=/1397/11/30/Chaartaar%20-%20Aavaazeh%20Khaan.mp3',
        'searched'  => 1
    ];
});

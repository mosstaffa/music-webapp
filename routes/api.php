<?php

use Illuminate\Http\Request;
Route::group([
    'prefix'    => 'songs',
    // 'middleware'    => 'auth:api'
], function (){
    Route::get('/weekly', 'SongController@weeklySongs');
    Route::get('/monthly', 'SongController@monthlySongs');
    Route::post('/search', 'SongController@search');
    Route::post('/getlinks', 'SongController@getLinks');
});
Route::group([
    'prefix' => 'auth'
], function (){
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');
    Route::get('/logout', 'AuthController@logout')->middleware('auth:api');
});

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="موزیک یاب اپلیکیشن جستجوی موزیک فارسی - ساده تر از همیشه موزیک های دلخواه خودت رو پیدا کن"/>
        <meta property="og:locale" content="fa_IR" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="موزیک یاب | جستجوی موزیک ساده تر از همیشه" />
        <meta property="og:description" content="موزیک یاب اپلیکیشن جستجوی موزیک فارسی - ساده تر از همیشه موزیک های دلخواه خودت رو پیدا کن" />
        <meta property="og:url" content="http://moozikyab.ir" />
        <meta property="og:site_name" content="نکس وان موزیک" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:description" content="موزیک یاب اپلیکیشن جستجوی موزیک فارسی - ساده تر از همیشه موزیک های دلخواه خودت رو پیدا کن" />
        <meta name="twitter:title" content="موزیک یاب | جستجوی موزیک ساده تر از همیشه" />
        <meta name="twitter:site" content="@moozikyab" />
        <title>موزیک یاب | جستجوی موزیک ساده تر از همیشه</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('css/style.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="icon" href="/images/logo.png">
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>
    <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>

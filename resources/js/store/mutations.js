export const mutations = {
    retreiveToken( state, props ) {
        state.token = props.access_token
        state.expires_at = props.expires_at
    },
    retreiveWeekly( state, props ){
        state.weeklyList = props
        console.log(state.weeklyList)
    },
    retreiveMonthly( state, props ){
        state.monthlyList = props
        console.log(state.monthlyList)
    }
}
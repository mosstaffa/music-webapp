export const getters = {
    loggedIn ( state ) {
        // get the expores_at 
        let time = new Date()
        let expires_at = new Date(state.expires_at)
        if( expires_at == null )
            return false
        if( expires_at > time ){
            if( state.token ){
                return true
            }
            return false
        }
        return false
    },
    weeklyList( state ){
        return state.weeklyList
    },
    monthlyList( state ){
        return state.monthlyList
    },
    token( state ){
        return state.token
    },
    apiKey( state ) {
        return state.token
    }
}
import axios from 'axios'

// axios.defaults.baseURL = 'http://moozikyab.ir/api'

axios.defaults.baseURL = 'http://127.0.0.1:8000/api';  
export const actions = {
    retreiveToken( context ){
        axios.post('/auth/login', {
            email: 'rhmustafaa@gmail.com',
            password: 'fklaew4pw2320ldasdalklwkeq23131',
            remember_me: true,
            app_key: context.state.apiKey
        })
        .then( res => {
            localStorage.setItem('token', res.data.body.access_token)
            localStorage.setItem('expires_at', res.data.body.expires_at)
            context.commit('retreiveToken', res.data.body)
        })
        .catch( err => console.log(err))
    },
    retreiveWeekly( context ){
        axios.defaults.headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + context.state.token
        }
        axios.get('/songs/weekly')
            .then( res => {
                context.commit('retreiveWeekly', res.data.body)
            })
            .catch( err => console.log( err ) )
    },
    retreiveMonthly( context ){
        axios.get('/songs/monthly', {
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization': 'Bearer ' + context.state.token
            }
        })
        .then( res => {
            context.commit('retreiveMonthly', res.data.body)
        })
        .catch( err => console.log( err ) )
    }
}

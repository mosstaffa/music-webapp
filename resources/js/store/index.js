import Vuex from "vuex"
import Vue from 'vue'
import { getters } from './getters'
import { mutations } from './mutations'
import { actions } from './actions'
Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        apiKey: 'sjdlslsdspewlskdjsnvk+/ds/+/dsda/=dsadalklxvls/ds=21ldsk',
        token: localStorage.getItem('token') || null,
        expires_at: localStorage.getItem('expires_at') || null,
        weeklyList: null,
        monthlyList: null,
    },
    getters,
    mutations,
    actions
})

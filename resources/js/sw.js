const cacheName = 'moozikyabCache-v1';
self.addEventListener('install', e => {
    const cacheReady = caches.open(cacheName).then( cache => {
        console.log('new cache ready');
        return cache.addAll(['/app.js', '/images/banner.svg', '/style.css', '/index.html']);
    });
    e.waitUntil(cacheReady);
});
self.addEventListener('activate', (e) => {
    let cacheCleaned = caches.keys().then(keys => {
        keys.forEach( key => {
            if(key !== pwaCache ) return caches.delete(key);
        });
    });
    e.waitUntil(cacheCleaned);
});